# README #

kafka_2.11-0.9.0.0 의 서버 configuration과 Producer , Consumer 예제 코드.
본 예제 코드는 3개의 partition을 가진 my-topic 이라는 topic을 생성하고 5개의 String 데이터를 카프카 브로커에 publishing 후 my-topic-group 이라는 Conumer로 subcribing 하는 예제 코드.
producer,consumer 코드외에 kafka.admin.AdminUtils 및 kafka.utils.ZkUtils 사용법을 참조할 수 있음.


![kafka_cluster_도식1.png](https://bitbucket.org/repo/6Gr8zo/images/934553130-kafka_cluster_%EB%8F%84%EC%8B%9D1.png)


![kafka_topic_replica_도식.png](https://bitbucket.org/repo/6Gr8zo/images/3548460640-kafka_topic_replica_%EB%8F%84%EC%8B%9D.png)


### Apache Kafka 0.9.0.0 변경점 ###
아파치 카프카 0.9.0.0 는 이전 버전 대비 인증, SSL레이어 추가 등 많은 변화가 있었습니다.
kafka topic 중심의 변경점은 다음과 같음.

변경

* 더 이상 Java 1.6 지원하지않음.

* 더 이상 Scala 2.9 지원하지 않음.

* 1000이상의 Broker ID는 자동으로 Broker ID를 할당하기 위해 예약되어 있음. 이미 1000 이상의 Broker ID를 사용하고 있다면 Broker Configuration의 reserved.broker.max.id 값을 임계값 이상으로 설정 해야 함.

* replica.lag.max.messages 설정은 제거되었으며, 동기화 되는 복제본 결정할 때 파티션 리더는 더 이상 후행(lag) 메시지의 수를 고려 하지 않음.

* replica.lag.time.max.ms 설정은 마지막 복제 요청으로 경과한 시간이 아닌 복제가 마지막으로 이뤄진 시간까지를 포함. 복제는 여전히 리더로부터 패치하며  replica.lag.time.max.ms 시간내에 복제가 되지 않으면 sync가 어긋난것으로 간주.

* log.cleaner.enable 는 true 가 기본값이 됨. 이는 cleanup.policy=compact 을 설정 시 topic은 기본적으로 compact 적용, 128MB 힙사이즈가 log.cleaner.dedupe.buffer.size 설정을 통한 clear process에게 할당. compacted topics의 사용량에 따라 log.cleaner.dedupe.buffer.size 와 다른 log.cleaner 계열의 설정을 조정해야 할 수 있음.

* MirrorMaker는 더 이상 multiple target clusters를 지원하지 않음. 그 결과로 단일 consumer.config 설정만 허용되며, 다중 소스 클러스터를 미러링 하려면 각각의 consumer configuration의 소스 클러스터당 적어도 하나 이상의 MirrorMaker 인스턴스가 필요함.

* org.apache.kafka.clients.tools.* 는 org.apache.kafka.tools.* 로 이관. 

* kafka-run-class.sh 내의 JVM 성능 옵션 (KAFKA_JVM_PERFORMANCE_OPTS) 변경.

* kafka-topics.sh 스크립트 ( kafka.admin.TopicCommand )는 이제 실행 실패 시 0이 아닌 종료 코드로 종료.

* kafka-topics.sh 스크립트 ( kafka.admin.TopicCommand )는 이제 '.' 이나 '_' 등 topic 이름이 충돌할 수 있는 경우 경고 메세지를 출력.

* kafka-console-producer.sh 스크립트 ( kafka.tools.ConsoleProducer )는 기본값으로 이전 생산자 대신 새 프로듀서 를 사용,  기존의 프로듀서를 사용하려면  이전버전의 producer 사용을 명시 해야 함.

* 기본적으로 명령줄 메세지는 stderr를 통해 출력.


삭제

* kafka-topics.sh script (kafka.admin.TopicCommand)를 통한 topic 변경 명령은 deprecate됨. 앞으로는 kafka-configs.sh script (kafka.admin.ConfigCommand)를 사용할 것.

* 오프셋을 확인용 명령 kafka-consumer-offset-checker.sh (kafka.tools.ConsumerOffsetChecker) 역시 deprecate됨. 

* kafka-consumer-groups.sh (kafka.admin.ConsumerGroupCommand)를 사용 할 것.

* kafka.tools.ProducerPerformance 클래스 deprecate 됨. org.apache.kafka.tools.ProducerPerformance 클래스를 사용 할 것. kafka-producer-perf-test.sh 역시 새로운 클래스 사용으로 변경 됨.