# Licensed to the Apache Software Foundation (ASF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The ASF licenses this file to You under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
# 
#    http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# see kafka.server.KafkaConfig for additional details and defaults

############################# Server Basics #############################

# The id of the broker. This must be set to a unique integer for each broker.
broker.id=4

############################# Socket Server Settings #############################

listeners=PLAINTEXT://:9094

# The port the socket server listens on
port=9094

# Hostname the broker will bind to. If not set, the server will bind to all interfaces
#host.name=localhost

# Hostname the broker will advertise to producers and consumers. If not set, it uses the
# value for "host.name" if configured.  Otherwise, it will use the value returned from
# java.net.InetAddress.getCanonicalHostName().
# advertised.host.name=192.168.60.12
advertised.host.name=192.168.204.126

# The port to publish to ZooKeeper for clients to use. If this is not set,
# it will publish the same port that the broker binds to.
advertised.port=9094

# The number of threads handling network requests
num.network.threads=3
 
# The number of threads doing disk I/O
num.io.threads=8

# The send buffer (SO_SNDBUF) used by the socket server
socket.send.buffer.bytes=102400

# The receive buffer (SO_RCVBUF) used by the socket server
socket.receive.buffer.bytes=102400

# The maximum size of a request that the socket server will accept (protection against OOM)
socket.request.max.bytes=104857600


############################# Log Basics #############################

# A comma seperated list of directories under which to store log files
log.dirs=/tmp/kafka-logs-9094

# The default number of log partitions per topic. More partitions allow greater
# parallelism for consumption, but this will also result in more files across
# the brokers.
num.partitions=10

# The number of threads per data directory to be used for log recovery at startup and flushing at shutdown.
# This value is recommended to be increased for installations with data dirs located in RAID array.
num.recovery.threads.per.data.dir=1

############################# Log Flush Policy #############################

# Messages are immediately written to the filesystem but by default we only fsync() to sync
# the OS cache lazily. The following configurations control the flush of data to disk. 
# There are a few important trade-offs here:
#    1. Durability: Unflushed data may be lost if you are not using replication.
#    2. Latency: Very large flush intervals may lead to latency spikes when the flush does occur as there will be a lot of data to flush.
#    3. Throughput: The flush is generally the most expensive operation, and a small flush interval may lead to exceessive seeks. 
# The settings below allow one to configure the flush policy to flush data after a period of time or
# every N messages (or both). This can be done globally and overridden on a per-topic basis.

# The number of messages to accept before forcing a flush of data to disk
#log.flush.interval.messages=10000

# The maximum amount of time a message can sit in a log before we force a flush
#log.flush.interval.ms=1000

############################# Log Retention Policy #############################

# The following configurations control the disposal of log segments. The policy can
# be set to delete segments after a period of time, or after a given size has accumulated.
# A segment will be deleted whenever *either* of these criteria are met. Deletion always happens
# from the end of the log.

# The minimum age of a log file to be eligible for deletion
#log.retention.hours=17520
log.retention.hours=2

# A size-based retention policy for logs. Segments are pruned from the log as long as the remaining
# segments don't drop below log.retention.bytes.
#log.retention.bytes=1073741824

# The maximum size of a log segment file. When this size is reached a new log segment will be created.
log.segment.bytes=536870912

# The interval at which log segments are checked to see if they can be deleted according 
# to the retention policies
log.retention.check.interval.ms=300000

# By default the log cleaner is disabled and the log retention policy will default to just delete segments after their retention expires.
# If log.cleaner.enable=true is set the cleaner will be enabled and individual logs can then be marked for log compaction.
log.cleaner.enable=true

############################# Zookeeper #############################

# Zookeeper connection string (see zookeeper docs for details).
# This is a comma separated host:port pairs, each corresponding to a zk
# server. e.g. "127.0.0.1:3000,127.0.0.1:3001,127.0.0.1:3002".
# You can also append an optional chroot string to the urls to specify the
# root directory for all kafka znodes.
zookeeper.connect=localhost:2181

# Timeout in ms for connecting to zookeeper
zookeeper.connection.timeout.ms=6000

# Zookeeper session timeout
zookeeper.session.timeout.ms=30000

# Enable auto creation of topic on the server
auto.create.topics.enable=true

# Enables auto leader balancing. 
# A background thread checks and triggers leader balance if required at regular intervals
auto.leader.rebalance.enable=true

# Specify the final compression type for a given topic. 
# This configuration accepts the standard compression codecs ('gzip', 'snappy', lz4). 
# It additionally accepts 'uncompressed' which is equivalent to no compression; 
# and 'producer' which means retain the original compression codec set by the producer.
compression.type=uncompressed

# Enables delete topic. Delete topic through the admin tool will have no effect if this config is turned off
delete.topic.enable=true

# The maximum time before a new log segment is rolled out (in hours), secondary to log.roll.ms property
log.roll.hours=17520

# Number of fetcher threads used to replicate messages from a source broker. 
# Increasing this value can increase the degree of I/O parallelism in the follower broker.
num.replica.fetchers=2

# Offset commit will be delayed until all replicas for the offsets topic receive the commit 
# or this timeout is reached. This is similar to the producer request timeout
offsets.commit.timeout.ms=6000

# If a follower hasn't sent any fetch requests or hasn't consumed up to the leaders log end offset 
# for at least this time, the leader will remove the follower from isr
replica.lag.time.max.ms=15000

# max wait time for each fetcher request issued by follower replicas. 
# This value should always be less than the replica.lag.time.max.ms at all times to prevent frequent 
# shrinking of ISR for low throughput topics
replica.fetch.wait.max.ms=1500

# Idle connections timeout: the server socket processor threads close the connections that idle more than this
connections.max.idle.ms=1200000

# The default cleanup policy for segments beyond the retention window, must be either "delete" or "compact"
log.cleanup.policy=compact

# The maximum number of connections we allow from each ip address
max.connections.per.ip=100

# How far a ZK follower can be behind a ZK leader
zookeeper.sync.time.ms=8000
