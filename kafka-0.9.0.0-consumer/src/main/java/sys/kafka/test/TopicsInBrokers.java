package sys.kafka.test;

import java.util.List;
import java.util.Set;
import kafka.utils.ZKStringSerializer$;
import kafka.utils.ZkUtils;
import org.I0Itec.zkclient.ZkClient;
import org.I0Itec.zkclient.ZkConnection;
import scala.collection.JavaConversions;

/**
 * TopicsInBrokers (UTF-8) created : 2016. 2. 16
 *
 * @golfzon.service
 * @golfzon.group
 * @golfzon.menu
 * @author shinys
 */
public class TopicsInBrokers {

    public static void main(String[] args) {
        
        String zookeepers = "localhost:2181";
        ZkUtils zkUtils = new ZkUtils(new ZkClient(zookeepers, 30000, 30000, ZKStringSerializer$.MODULE$)
                , new ZkConnection(zookeepers, 30000)
                , false);
        
        List<String> allTopics = JavaConversions.seqAsJavaList(zkUtils.getAllTopics());
        for (String topic : allTopics) {
            System.out.println("Topic : " + topic);
            Set<String> allConsumerGroups = JavaConversions.setAsJavaSet(zkUtils.getAllConsumerGroupsForTopic(topic) );
            for( String cg : allConsumerGroups){
                System.out.println("\tConsumerGroupId : " + cg);
            }
        }
        zkUtils.close();
    }
}
