package sys.kafka;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.LongSerializationPolicy;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.apache.kafka.clients.consumer.CommitFailedException;
import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.InvalidOffsetException;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;

import org.apache.kafka.common.KafkaException;
import org.apache.kafka.common.TopicPartition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Consumer (UTF-8) created : 2016. 2. 15
 *
 * @golfzon.service
 * @golfzon.group
 * @golfzon.menu
 * @author shinys
 */
public class Consumer implements AutoCloseable {

    private static final Logger LOGGER = LoggerFactory.getLogger(Consumer.class);
    private Gson _gson;
    private String _topic;
    private String _broker;
    private String _groupId;
    private String _cilentId;
    private static final long SESSION_TIMEOUT = 10000;
    private static final long READ_TIMEOUT = 3000;
    private KafkaConsumer<String, String> _consumer;    
    public final static AtomicLong ATOMICLONG = new AtomicLong(0); 
    // Java8 환경에서 운영 한다면 StampedLock으로 변경을 고려할 것.
    private final ReadWriteLock lock = new ReentrantReadWriteLock();

    private Consumer() {
    }

    /**
     * 
     * @param brokers kafka브로커 host:port
     * @param topic 조회 하고자 하는 topic 명
     * @param consumerGroupId  컨슈머그룹ID
     */
    public Consumer(String brokers, String topic, String consumerGroupId){
        this(brokers, topic, consumerGroupId, topic+"-client-"+ATOMICLONG.incrementAndGet());
    }
    
    
    /**
     * 
     * @param brokers kafka브로커 host:port
     * @param topic 조회 하고자 하는 topic 명
     * @param consumerGroupId 컨슈머그룹ID
     * @param consumerId 컨슈머 ID
     */
    public Consumer(String brokers, String topic, String consumerGroupId, String consumerId) {
        this._gson = new GsonBuilder()
                .serializeNulls()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .setLongSerializationPolicy(LongSerializationPolicy.STRING)
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .serializeSpecialFloatingPointValues()
                .serializeNulls()
                //.setPrettyPrinting()
                //.registerTypeAdapter(Date.class, new DateDeserializer())
                //.registerTypeAdapter(Timestamp.class, new DateDeserializer())
                .create();
        this._topic = topic;
        this._broker = brokers;
        this._groupId = consumerGroupId;
        this._cilentId = consumerId;
        
        this._consumer = new KafkaConsumer<String, String>(props());
        this._consumer.subscribe(Arrays.asList(topic.split(",")) , new ConsumerRebalanceListener(){
            @Override
            public void onPartitionsRevoked(Collection<TopicPartition> clctn) {
                LOGGER.info("Consumer PartitionsRevoked");
                for(TopicPartition p : clctn){
                    LOGGER.info( "\ttopic : "+p.topic() +", partition : "+p.partition() );
                }
            }

            @Override
            public void onPartitionsAssigned(Collection<TopicPartition> clctn) {
                LOGGER.info("Consumer PartitionsAssigned");
                for(TopicPartition p : clctn){
                    LOGGER.info( "\ttopic : "+p.topic() +", partition : "+p.partition() );
                }
            }
        } );

    }
    
    /**
     * http://kafka.apache.org/documentation.html#newconsumerconfigs
     * @return 
     */
    private Properties props(){
        Properties props = new Properties();
        //http://people.apache.org/~nehanarkhede/kafka-0.9-producer-javadoc/doc/org/apache/kafka/clients/consumer/KafkaConsumer.html
        props.put("bootstrap.servers", this._broker);
        if( this._groupId!=null )
            props.put("group.id", this._groupId);
        if( this._cilentId!=null )
            props.put("client.id", this._cilentId);
        //autocomit은 사용하지 않음.
        props.put("enable.auto.commit", "false");
        //The timeout used to detect failures when using Kafka's group management facilities.
        props.put("session.timeout.ms", String.valueOf(SESSION_TIMEOUT) );
        //request.timeout.ms should be greater than session.timeout.ms and fetch.max.wait.ms
        props.put("request.timeout.ms", String.valueOf( (long)(SESSION_TIMEOUT*1.5) )  );
        //consumer's session stays active and to facilitate rebalancing when new consumers join or leave the group. session.timeout.ms 보다는 낮은값이어야 함 1/3 이하를 권장.
        //For larger groups, it may be wise to increase this setting.
        props.put("heartbeat.interval.ms", String.valueOf( (long)(SESSION_TIMEOUT/4) ) );
        //초기 offset값이 설정되지 않았거나 offset 값이 존재하지 않는경우.
        props.put("auto.offset.reset", "earliest");    //    latest  , earliest
        props.put("connections.max.idle.ms", "540000");        
        props.put("receive.buffer.bytes", "32768");
        props.put("send.buffer.bytes", "131072");        
        props.put("reconnect.backoff.ms", "500");
        props.put("retry.backoff.ms", "1000");        
        props.put("fetch.max.wait.ms", "3000");
        props.put("fetch.min.bytes", "1");
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        return props;
    }

    /**
     *
     * @param <T>
     * @param classOfT
     * @return
     * @throws InvalidOffsetException if the offset for a partition or set of
     * partitions is undefined or out of range and no offset reset policy has
     * been configured
     * @throws KafkaException for any other unrecoverable errors (e.g. invalid
     * groupId or session timeout, errors deserializing key/value pairs, or any
     * new error cases in future versions)
     * @throws NoSuchElementException  토픽에서 timeout 시간 동안 조회할 데이터가 없는경우. NoSuchElementException throw.
     */
    public <T extends Object> T nextData(Class<T> classOfT) throws InvalidOffsetException, KafkaException,NoSuchElementException {
        return nextData((Type)classOfT, READ_TIMEOUT);
    }
    
    /**
     * 
     * @param <T>
     * @param classOfT
     * @param readTimeout data poll timeout 밀리세컨드.
     * @return
     * @throws InvalidOffsetException
     * @throws KafkaException
     * @throws NoSuchElementException 
     */
    public <T extends Object> T nextData(Class<T> classOfT, long readTimeout) throws InvalidOffsetException, KafkaException,NoSuchElementException {
        return nextData((Type)classOfT, readTimeout);
    }
    
    /**
     * 
     * @param <T>
     * @param typeOfT The specific genericized type of src. You can obtain this type by using the com.google.gson.reflect.TypeToken class. For example, to get the type for Collection&lt;Foo&gt;, you should use:<br >
     * Type typeOfT = new TypeToken&lt;Collection&lt;Foo&gt;&gt;(){}.getType();
     * @return
     * @throws InvalidOffsetException
     * @throws KafkaException
     * @throws NoSuchElementException 
     */
    public <T extends Object> T nextData(Type typeOfT) throws InvalidOffsetException, KafkaException,NoSuchElementException {
        return nextData(typeOfT, READ_TIMEOUT);
    }
    

    /**
     *
     * @param <T>
     * @param typeOfT The specific genericized type of src. You can obtain this type by using the com.google.gson.reflect.TypeToken class. For example, to get the type for Collection&lt;Foo&gt;, you should use:<br >
     * Type typeOfT = new TypeToken&lt;Collection&lt;Foo&gt;&gt;(){}.getType();
     * @param readTimeout data poll timeout 밀리세컨드.
     * @return
     * @throws InvalidOffsetException if the offset for a partition or set of partitions is undefined or out of range and no offset reset policy has been configured
     * @throws KafkaException for any other unrecoverable errors (e.g. invalid groupId or session timeout, errors deserializing key/value pairs, or any new error cases in future versions)
     * @throws NoSuchElementException  토픽에서 timeout 시간 동안 조회할 데이터가 없는경우. NoSuchElementException throw.
     */
    public <T extends Object> T nextData(Type typeOfT, long readTimeout) throws InvalidOffsetException, KafkaException, NoSuchElementException {
        T result = null;
        lock.writeLock().lock();
        try{
            if(iter == null || !iter.hasNext() ){
            //if(records == null || records.isEmpty() ){
                //consumer.poll() 하는동안 heartbeat이 수행 됨. http://www.confluent.io/blog/tutorial-getting-started-with-the-new-apache-kafka-0.9-consumer-client
                ConsumerRecords<String, String> records = this._consumer.poll(readTimeout);
                LOGGER.info("record size : "+records.count());
                iter = records.iterator();
            }
            String currentTopic = null;
            try {
                if( iter!=null && iter.hasNext()){
                //for (ConsumerRecord<String, String> record : records) {
                    ConsumerRecord<String, String> record = iter.next();                    
                    currentTopic = record.topic();
                    if( typeOfT == String.class ){                    
                        result = (T) record.value();
                    }else if( typeOfT == Integer.class ){  
                        result = (T) Integer.valueOf( record.value() );
                    }else if( typeOfT == Long.class ){  
                        result = (T) Long.valueOf( record.value() );
                    }else if( typeOfT == Double.class ){  
                        result = (T) Double.valueOf( record.value() );
                    }else if( typeOfT == Float.class ){  
                        result = (T) Float.valueOf( record.value() );
                    }else if( typeOfT == Boolean.class ){  
                        result = (T) Boolean.valueOf( record.value() );
                    }else{
                        result = this._gson.fromJson(record.value(), typeOfT);
                    }
                    LOGGER.info( String.format("topic = %s, partition = %d, offset = %d, key = %s, value = %s",record.topic(), record.partition() , record.offset(), record.key(), record.value() ) );
                    this._consumer.commitSync( Collections.singletonMap( new TopicPartition(record.topic(), record.partition() ), new OffsetAndMetadata(record.offset() + 1)));
                    //_consumer.commitSync();
                    //break;
                }else{
                    throw new NoSuchElementException( "컨슈머 그룹 "+this._groupId +"의 조회 토픽 "+ this._topic +" 내에 더 이상 데이터가 없을 수 있음.("+readTimeout+"ms 동안 데이터 poll 안됨)");
                }
            } catch (CommitFailedException e) {
                //LOGGER.error(_topic,e);
                throw new KafkaException("Maybe consumer session timeout("+SESSION_TIMEOUT+"ms) occured and connection released. (group.id : "+this._groupId+", client.id : "+ this._cilentId+ ", topic : "+currentTopic+")",e );
            } catch(Exception e){
                throw new RuntimeException("컨슈머에서 데이터 처리 중 오류 (group.id : "+this._groupId+", client.id : "+ this._cilentId+ ", topic : "+currentTopic+")",e);
            }
        }finally{
            lock.writeLock().unlock();
        }
        return result;
    }
    
    private Iterator<ConsumerRecord<String,String>> iter =null;
    
    public String getTopic(){
        return this._topic;
    }
    
    public String getConsumerGroupId(){
        return this._groupId;
    }
    
    public String getConsumerId(){
        return this._cilentId;
    }
    
    public String getBroker(){
        return this._broker;
    }
    

    @Override
    public void close() throws Exception {
        if (this._consumer != null) {
            this._consumer.unsubscribe();
            this._consumer.close();
        }
    }

}
