package sys.kafka;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.LongSerializationPolicy;
import java.io.UnsupportedEncodingException;
import java.util.Properties;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import kafka.admin.AdminUtils;
import kafka.api.TopicMetadata;
import kafka.utils.ZKStringSerializer$;
import kafka.utils.ZkUtils;
import org.I0Itec.zkclient.ZkClient;
import org.I0Itec.zkclient.ZkConnection;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Producer (UTF-8) created : 2016. 2. 15
 *
 * @param <D>
 * @golfzon.service
 * @golfzon.group
 * @golfzon.menu
 * @author shinys
 */
public class Producer<D extends Object> implements AutoCloseable{
    private static final Logger LOGGER = LoggerFactory.getLogger(Producer.class);

    private KafkaProducer _producer;
    private Gson _gson;
    private Properties _properties;
    private String _topic;
    private String _zookeepers;
    private String _brokers;
    private int _partitionSize;
    private AtomicLong _adder = null;
    
    private Producer() {
    }

    /**
     * 
     * @param zookeepers 주키퍼목록
     * @param brokers 카프카브로커목록
     * @param topic  메세지를 발행 할 토픽이름
     * @param partitionSize 토픽의 파티션크기. 이미 존재하는 토픽이라면 본 설정 값은 무효.
     */
    public Producer( String zookeepers, String brokers, final String topic, int partitionSize ) {        
        this._gson = new GsonBuilder()
                .serializeNulls()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .setLongSerializationPolicy(LongSerializationPolicy.STRING)
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .serializeSpecialFloatingPointValues()
                .serializeNulls()
                //.setPrettyPrinting()
                //.registerTypeAdapter(Date.class, new DateDeserializer())
                //.registerTypeAdapter(Timestamp.class, new DateDeserializer())
                .create();
        this._zookeepers = zookeepers;
        this._brokers = brokers;
        this._topic =topic;
        this._partitionSize = partitionSize;
        this._properties = props();
        this._adder = new AtomicLong();
        prepaire();
    }
    
    /**
     * 프로듀서 전체 설정 설명은 http://kafka.apache.org/documentation.html#producerconfigs 
     * @return 
     */
    private Properties props(){
        Properties props = new Properties();
        // 콤마로 구분된 Kafka cluster목록
        props.put("bootstrap.servers", this._brokers);
        // 발행 후 복제 싱크 확인 수준 결정. 0:데이타 수신 후 복제 완료를 기다리지 않고 응답, retries 설정은 무시됨. 1:리더노드log기록은완료하지만 모든 folowe에 대한 싱크는 기다리지 않고 응답. all:모든 복제 싱크를 기다린 후 응답
        props.put("acks", "all");
        props.put("retries", 0);
        props.put("batch.size", 16384);
        props.put("linger.ms", 1);
        //send() 메소드와 partitionsFor()을 처리를 기다리는 시간
        props.put("max.block.ms", 10000);
        //서버에 보내기전에 버퍼링 가능한 최대 bytes. block.on.buffer.full 설정에따라 버퍼가 비길 기다리거나 익셉션을 던짐.
        props.put("buffer.memory", 33554432);        
        props.put("block.on.buffer.full", false);
        //설정시간이후동안 유휴 상태인 커넥션은 close 처리.
        props.put("connections.max.idle.ms", 540000);
        //응답을 기다리는 최대 시간. 설정시간까지 응답을 받지 못하면 요청을 재전송하거나 retries횟수를 넘기면 fail처리.
        props.put("request.timeout.ms", 60000);
        //TCP 수신버퍼(SO_RCVBUF)
        props.put("receive.buffer.bytes", 32768);
        //TCP 요청버퍼(SO_SNDBUF)
        props.put("send.buffer.bytes", 131072);        
        props.put("reconnect.backoff.ms", 500);
        props.put("retry.backoff.ms", 700);
        //메세지를 저장할 파티션 선택을 담당하는 클래스 지정.
        props.put("partitioner.class", "sys.kafka.RoundRobinPartitioner");
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        return props;
    }

    private void prepaire() {
        //kafak 연결, 토픽 생성
        ZkClient zkClient = new ZkClient(
                this._zookeepers
                , 60000
                , 60000
                , ZKStringSerializer$.MODULE$);
        ZkUtils zkUtils = new ZkUtils(zkClient, new ZkConnection(this._zookeepers, 30000), false);
        if (!AdminUtils.topicExists(zkUtils, _topic)) {
            LOGGER.info( "카프카 브로커 토픽 생성 : " + _topic );
            int replicationFactor = this._brokers.split(",").length;
            AdminUtils.createTopic(zkUtils, _topic, _partitionSize, replicationFactor, new Properties());
        }else{
            LOGGER.warn("카프카 브로커 토픽 "+_topic+ "은 이미 존재.");
            TopicMetadata topicMetadata = AdminUtils.fetchTopicMetadataFromZk(_topic, zkUtils);
            this._partitionSize = topicMetadata.partitionsMetadata().size();
        }
        zkUtils.close();
        zkClient.close();
        //kafka producer 준비
        this._producer = new KafkaProducer(this._properties);        
    }
    
    public Future<RecordMetadata> send(D message,Callback callback){
        if( LOGGER.isInfoEnabled() )
            LOGGER.info( "메세지 발행 topic : " + _topic + ", message : "+ message );
        
        Future<RecordMetadata> future = null;

        if( message instanceof String ){            
            future = this._producer.send( new ProducerRecord<String, String>( _topic, (_topic + (this._adder.getAndIncrement()%(long)this._partitionSize) ), (String)message ) , callback);            
        }else{
            String msg = _gson.toJson(message);
            future = this._producer.send( new ProducerRecord<String, String>( _topic, (_topic + (this._adder.getAndIncrement()%(long)this._partitionSize) ), msg ) , callback);
        }

        this._producer.flush();
        return future;
    }
    
    public Future<RecordMetadata> send(D message){
        return send( message , null );
    }
    

    @Override
    public void close() throws Exception {
        if(this._producer!=null)
            this._producer.close(2000, TimeUnit.MILLISECONDS);
    }

}
