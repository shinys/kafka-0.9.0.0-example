package sys.kafka.test;

import sys.kafka.Producer;

/**
 * ProducerTest (UTF-8)
 * created : 2016. 2. 15
 * 
 * @golfzon.service 
 * @golfzon.group 
 * @golfzon.menu 
 * @author shinys
 */
public class ProducerTest {
    
    public static void main(String[] args) throws Exception {
        
        String zookeepers = "localhost:2181";
        String brokers = "192.168.204.126:9092,192.168.204.126:9093,192.168.204.126:9094";
        String topic = "my-topic";        
        int partitionSize = 3; //동일 group.id 의 컨슈머 최대 갯수가 제약됨
        
        try (Producer<String> p = new Producer<String>(zookeepers, brokers, topic, partitionSize)) {
            for(int i = 10 ; i < 15 ; i++){
                //System.out.println(i+"번째\t 메세지.");
                p.send( i+"번째\t 메세지.");
            }
        }
        
    }

}
